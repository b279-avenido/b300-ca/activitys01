<?php


function address($street,$city,$country){
	return "$street, $city, $country";
}

function grading($score){
	if($score < 75){
		return 'F';
	}else if($score >= 75 && $score <= 78){
		return 'E';
	}else if($score >= 79 && $score <= 83){
		return 'D';
	}else if($score >= 84 && $score <= 88){
		return 'C';
	}else if($score >= 89 && $score <= 93){
		return 'B';
	}else{
		return 'A-';
	}
}



